@echo off
%~d0
cd %~p0

call mvn clean install -Dmaven.test.skip=true
if errorlevel 1 goto error
if errorlevel 0 goto success

:success
	echo Build success
	pause
	goto end
:error
	echo Build fails
	pause
:end
