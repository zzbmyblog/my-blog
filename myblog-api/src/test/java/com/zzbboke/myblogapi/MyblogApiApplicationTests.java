package com.zzbboke.myblogapi;

import com.zzbboke.myblogapi.constant.ResponseCode;
import com.zzbboke.myblogapi.entity.MyblogUserEntity;
import com.zzbboke.myblogapi.repository.MyblogUserRepository;
import com.zzbboke.myblogapi.service.impl.MyblogUserServiceImpl;
import com.zzbboke.myblogapi.vo.Response;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Slf4j
class MyblogApiApplicationTests {
    //
    @Autowired
    private MyblogUserServiceImpl myblogUserService;

    @Autowired
    private MyblogUserRepository myblogUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;




    @Test
    void contextLoads() {
    }

//    @Test
//    public void Test(){
//        final List<UserVO> userVOS = userServiceimpl.listAll();
//        log.info("userVOS="+ Arrays.toString(userVOS.toArray()));
//    }

    @Test
    @Transactional
    public void findById() {
        myblogUserService.fetchById(1L).ifPresent(a -> {
         log.info("a"+a.getMenuList());
        });
    }

    @Test
    @Transactional
    @Rollback(value = false)
    public void inset(){
        MyblogUserEntity myblogUserEntity =new MyblogUserEntity();
        myblogUserEntity.setUsername("2417672197");
        BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder();
        myblogUserEntity.setPassword(bCryptPasswordEncoder.encode("123456"));
        myblogUserEntity.setEmailAddress("2417672197@qq.com");
        myblogUserService.create(myblogUserEntity);
    }


    @Test
    @Transactional
    @Rollback(value = false)
    public void update(){
        Response<String> aa = Response.from(ResponseCode.SUCCESS, null);
        log.info("aa");
    }

}
