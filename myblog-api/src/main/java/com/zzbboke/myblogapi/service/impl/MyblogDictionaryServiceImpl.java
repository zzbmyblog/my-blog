package com.zzbboke.myblogapi.service.impl;
import org.springframework.stereotype.Service;

import com.zzbboke.myblogapi.entity.MyblogDictionaryEntity;
import com.zzbboke.myblogapi.vo.MyblogDictionaryVO;
import com.zzbboke.myblogapi.dto.MyblogDictionaryDTO;
import com.zzbboke.myblogapi.service.MyblogDictionaryService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;


import javax.annotation.Resource;
import java.util.List;

/**
 * 数据字典分类表(MyblogDictionary)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service("myblogDictionaryService")
public class MyblogDictionaryServiceImpl extends AbstractCrudService<MyblogDictionaryVO, MyblogDictionaryEntity, MyblogDictionaryDTO,Long> implements MyblogDictionaryService {
   
}