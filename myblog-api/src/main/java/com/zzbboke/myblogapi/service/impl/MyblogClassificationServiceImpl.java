package com.zzbboke.myblogapi.service.impl;
import org.springframework.stereotype.Service;

import com.zzbboke.myblogapi.entity.MyblogClassificationEntity;
import com.zzbboke.myblogapi.vo.MyblogClassificationVO;
import com.zzbboke.myblogapi.dto.MyblogClassificationDTO;
import com.zzbboke.myblogapi.service.MyblogClassificationService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;


import javax.annotation.Resource;
import java.util.List;

/**
 * 菜单分类表(MyblogClassification)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service("myblogClassificationService")
public class MyblogClassificationServiceImpl extends AbstractCrudService<MyblogClassificationVO, MyblogClassificationEntity, MyblogClassificationDTO,Long> implements MyblogClassificationService {
   
}