package com.zzbboke.myblogapi.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import com.zzbboke.myblogapi.dto.LoginDTO;
import com.zzbboke.myblogapi.dto.MyblogUserDTO;
import com.zzbboke.myblogapi.entity.MyblogResourcesEntity;
import com.zzbboke.myblogapi.entity.MyblogRolesEntity;
import com.zzbboke.myblogapi.entity.MyblogUserEntity;
import com.zzbboke.myblogapi.exception.NotFoundDataException;
import com.zzbboke.myblogapi.repository.MyblogUserRepository;
import com.zzbboke.myblogapi.service.MyblogUserService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;
import com.zzbboke.myblogapi.vo.AuthToken;
import com.zzbboke.myblogapi.vo.MyblogUserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.HashMap;
import java.util.Optional;

/**
 * 用户信息(MyblogUser)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service
public class MyblogUserServiceImpl extends AbstractCrudService<MyblogUserVO, MyblogUserEntity, MyblogUserDTO, Long> implements MyblogUserService {

    @Autowired
    private MyblogUserRepository myblogUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void customizeToVO(MyblogUserEntity myblogUserEntity, MyblogUserVO myblogUserVO) {
        super.customizeToVO(myblogUserEntity, myblogUserVO);
        HashMap<String, String> hashMap = new HashMap();
        if (CollectionUtil.isEmpty(myblogUserEntity.getMyblogRoles())) {
            return;
        }
        for (MyblogRolesEntity myblogRolesEntity : myblogUserEntity.getMyblogRoles()) {
            if (CollectionUtil.isEmpty(myblogRolesEntity.getMyblogResourcesEntities())) {
                return;
            }
            for (MyblogResourcesEntity myblogResourcesEntity : myblogRolesEntity.getMyblogResourcesEntities()) {
                hashMap.put(myblogResourcesEntity.getResourcesCode(), myblogResourcesEntity.getUrl());
            }
        }
        myblogUserVO.setMenuList(Collections.singletonList(hashMap));

    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<MyblogUserEntity> byUsername = myblogUserRepository.findByUsername(username);
        return myblogUserRepository.findByUsername(username).orElseThrow(() -> new NotFoundDataException(username + "数据不存在!"));
    }


    @Override
    public AuthToken authCodeCheck(LoginDTO loginDTO) {
        return new AuthToken("aa", 1, "asd");
    }

    @Override
    public MyblogUserVO create(MyblogUserEntity myblogUserEntity) {
        if (!isExistUserName(myblogUserEntity.getUsername())) {
            throw new RuntimeException("此用户名已被注册!");
        }
        myblogUserEntity.setPassword(passwordEncryption(myblogUserEntity.getPassword()));
        return super.create(myblogUserEntity);
    }

    @Override
    public MyblogUserVO update(MyblogUserEntity myblogUserEntity) {
        myblogUserEntity.setPassword(passwordEncryption(myblogUserEntity.getPassword()));
        return super.update(myblogUserEntity);
    }

    @Override
    public Boolean isExistUserName(@NonNull String username) {
        Assert.notNull(username, "username不能为null!");
        return !myblogUserRepository.findByUsername(username).isPresent();
    }

    public String passwordEncryption(@NonNull String password) {
        Assert.notNull(password, "password不能为null!");
        return passwordEncoder.encode(password);
    }

    @Override
    public Boolean isPasswordCorrect(@NonNull String password) {
        Assert.notNull(password, "password不能为null!");
        return myblogUserRepository.findByPassword(passwordEncryption(password)).isPresent();
    }

    @Override
    public Boolean isExistEmailAddress(@NonNull String emailAddress) {
        Assert.notNull(emailAddress, "emailAddress不能为null!");
        return myblogUserRepository.findByEmailAddress(emailAddress).isPresent();
    }

    @Override
    public Boolean isExistActivationCode(@NonNull String activationCode) {
        Assert.notNull(activationCode, "activationCode不能为null!");
        return myblogUserRepository.findByActivationCode(activationCode).isPresent();
    }

    @Override
    public MyblogUserVO findByUsername(@NonNull String username) {
        Assert.notNull(username, "username不能为null!");
        return this.toVo(myblogUserRepository.findByUsername(username).orElseThrow(() -> new NotFoundDataException(username + "数据不存在!")));
    }

    @Override
    public MyblogUserVO findByEmailAddress(@NonNull String emailAddress){
        Assert.notNull(emailAddress, "emailAddress不能为null!");
        return this.toVo(myblogUserRepository.findByEmailAddress(emailAddress).orElseThrow(() -> new NotFoundDataException(emailAddress + "数据不存在!")));
    }

    @Override
    public Boolean isExistUsernameAndPassword(String username, String password) {
        Assert.notNull(username, "username不能为null!");
        Assert.notNull(password, "password不能为null!");
        if (this.isExistUserName(username)&&this.isPasswordCorrect(password)){
            return true;
        }
        return false;
    }
}