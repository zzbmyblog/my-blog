package com.zzbboke.myblogapi.service.impl;
import org.springframework.stereotype.Service;

import com.zzbboke.myblogapi.entity.MyblogContactEntity;
import com.zzbboke.myblogapi.vo.MyblogContactVO;
import com.zzbboke.myblogapi.dto.MyblogContactDTO;
import com.zzbboke.myblogapi.service.MyblogContactService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;


import javax.annotation.Resource;
import java.util.List;

/**
 * 联系方式(MyblogContact)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service("myblogContactService")
public class MyblogContactServiceImpl extends AbstractCrudService<MyblogContactVO, MyblogContactEntity, MyblogContactDTO,Long> implements MyblogContactService {
   
}