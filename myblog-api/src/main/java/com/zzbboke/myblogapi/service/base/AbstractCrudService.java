package com.zzbboke.myblogapi.service.base;

import cn.hutool.core.lang.Assert;
import com.google.common.collect.Lists;
import com.querydsl.core.types.Predicate;
import com.zzbboke.myblogapi.exception.NotFoundDataException;
import com.zzbboke.myblogapi.repository.base.BaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.*;

@Service
public abstract class AbstractCrudService<VO, DOMAIN, DTO, ID extends Serializable>
        extends ConversionServiceImpl<DOMAIN, VO, DTO> implements CrudService<VO, DOMAIN, DTO, ID> {

    @Autowired
    private BaseRepository<DOMAIN, ID> baseRepository;

    @Override
    public List<VO> listAll() {
        return this.toListVo(baseRepository.findAll());
    }

    @Override
    public List<VO> listAll(Sort sort) {
        Assert.notNull(sort, "sort  is not null!");
        return this.toListVo(baseRepository.findAll(sort));
    }

    @Override
    public List<VO> listAll(Predicate predicate) {
        Assert.notNull(predicate, "predicate  is not null!");
        return this.toListVo(Lists.newArrayList(baseRepository.findAll(predicate)));
    }

    @Override
    public Page<VO> listAll(Pageable pageable) {
        Assert.notNull(pageable, "sort  is not null!");
        return this.toPageVo(baseRepository.findAll(pageable));
    }

    @Override
    public Page<VO> listAll(Sort sort, Pageable pageable) {
        Assert.notNull(sort, "sort  is not null!");
        Assert.notNull(pageable, "pageable  is not null!");
        Assert.isTrue(pageable.getPageNumber() <= 0, "PageNumber不能<=0");
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber() - 1, pageable.getPageSize(), sort);
        return this.toPageVo(baseRepository.findAll(pageRequest));
    }

    @Override
    public Page<VO> listAll(Pageable pageable, Predicate predicate) {
        Assert.notNull(pageable, "pageable  is not null!");
        return this.toPageVo(baseRepository.findAll(predicate, pageable));

    }

    @Override
    public List<VO> listAllByIds(Collection<ID> ids) {
        return CollectionUtils.isEmpty(ids) ? Collections.emptyList() : this.toListVo(baseRepository.findAllById(ids));
    }

    @Override
    public List<VO> listAllByIds(Collection<ID> ids, Sort sort) {
        Assert.notNull(sort, "sort  is not null!");
        return CollectionUtils.isEmpty(ids) ? Collections.emptyList()
                : this.toListVo(baseRepository.findAllByIdIn(ids, sort));
    }

    @Override
    public Optional<VO> fetchById(ID id) {
        Assert.notNull(id, "id  is not null!");
        final Optional<DOMAIN> byId = baseRepository.findById(id);
        final DOMAIN domain = byId.orElseThrow(() -> new NotFoundDataException("此id:" + id + "数据不存在!"));
        return Optional.of(toVo(domain));
    }

    @Override
    public VO getById(ID id) {
        Assert.notNull(id, "id  is not null!");
        return this.fetchById(id).get();
    }

    @Override
    public boolean existsById(ID id) {
        Assert.notNull(id, "id  is not null!");
        return baseRepository.existsById(id);
    }

    @Override
    public VO create(DOMAIN domain) {
        Assert.notNull(domain, "domain  is not null!");
        return this.toVo(baseRepository.save(domain));
    }

    @Override
    public List<VO> createAll(Collection<DOMAIN> domains) {
        return domains.isEmpty() ? Collections.emptyList() : this.toListVo(baseRepository.saveAll(domains));
    }

    @Override
    public VO update(DOMAIN domain) {
        Assert.notNull(domain, "domain  is not null!");
        return this.toVo(baseRepository.save(domain));

    }

    @Override
    public List<VO> updateAll(Collection<DOMAIN> domains) {
        return domains.isEmpty() ? Collections.emptyList() : this.toListVo(baseRepository.saveAll(domains));
    }

    public boolean id(ID id) {
        Assert.notNull(id, "id  is not null!");
        return this.fetchById(id).isPresent();
    }

    @Override
    public void removeByDomain(DOMAIN domain) {
        Assert.notNull(domain, "domain  is not null!");
        baseRepository.delete(domain);
    }

    @Override
    public void removeByIds(Collection<ID> ids) {
        Assert.notNull(ids, "ids  is not null!");
        Iterator<ID> idLst = ids.iterator();
        while (idLst.hasNext()) {
            ID id = idLst.next();
            this.removeByIds(id);
        }
    }

    @Override
    public void removeByIds(ID id) {
        Assert.notNull(id, "ids  is not null!");
        baseRepository.deleteById(id);
    }

    @Override
    public List<VO> findAll(Predicate predicate) {
        Assert.notNull(predicate, "predicate  is not null!");
        List<DOMAIN> domainList = (List<DOMAIN>) baseRepository.findAll(predicate);
        return this.toListVo(domainList);
    }

    @Override
    public List<VO> findAll(Predicate predicate, Pageable pageable) {
        Assert.notNull(predicate, "predicate  is not null!");
        Assert.notNull(pageable, "pageable  is not null!");
        Page<DOMAIN> domainList = baseRepository.findAll(predicate, pageable);
        return this.toListVo(domainList.getContent());
    }
}
