package com.zzbboke.myblogapi.service.base;

import org.springframework.data.domain.Page;
import org.springframework.lang.NonNull;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ConversionService<DOMAIN, VO, DTO> {

	public Class<VO> getVoGenericClass();

	public Class<DTO> getDtoGenericClass();

	public Class<DOMAIN> getDomainGenericClass();

	@NonNull
	public List<VO> toListVo(Collection<DOMAIN> domainList);

	@NonNull
	public Page<VO> toPageVo(Page<DOMAIN> domainPage);

	@NonNull
	public VO toVo(DOMAIN domain);

	@NonNull
	public Optional<VO> toOptionalVo(Optional<DOMAIN> domainOptional);


	@NonNull
	public DTO toDto(VO vo);

	@NonNull
	public DOMAIN toDomain(DTO dto);

	void customizeToVO(DOMAIN domain, VO vo);


	void customizeToDTO(DOMAIN domain, DTO dto);

	void customizeToDOMAIN(DTO dto,DOMAIN domain);
}
