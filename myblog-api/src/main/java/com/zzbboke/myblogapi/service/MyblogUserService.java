package com.zzbboke.myblogapi.service;

import com.zzbboke.myblogapi.dto.LoginDTO;
import com.zzbboke.myblogapi.dto.MyblogUserDTO;
import com.zzbboke.myblogapi.entity.MyblogUserEntity;
import com.zzbboke.myblogapi.service.base.CrudService;
import com.zzbboke.myblogapi.vo.AuthToken;
import com.zzbboke.myblogapi.vo.MyblogUserVO;
import org.springframework.lang.NonNull;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 用户信息(MyblogUser)表服务接口
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */

public interface MyblogUserService extends CrudService<MyblogUserVO, MyblogUserEntity, MyblogUserDTO,Long>, UserDetailsService {

    @NonNull
    AuthToken authCodeCheck(@NonNull LoginDTO loginDTO);

    @NonNull
    Boolean isExistUserName(String username);

    @NonNull
    Boolean isPasswordCorrect(String password);

    @NonNull
    Boolean isExistEmailAddress(String emailAddress);

    @NonNull
    Boolean isExistActivationCode(String activationCode);

    @NonNull
    MyblogUserVO findByEmailAddress(@NonNull String emailAddress);

    @NonNull
    MyblogUserVO findByUsername(@NonNull String username);

    Boolean isExistUsernameAndPassword(@NonNull String username,@NonNull String password);



}