package com.zzbboke.myblogapi.service.impl;
import org.springframework.stereotype.Service;

import com.zzbboke.myblogapi.entity.MyblogDictionayInfoEntity;
import com.zzbboke.myblogapi.vo.MyblogDictionayInfoVO;
import com.zzbboke.myblogapi.dto.MyblogDictionayInfoDTO;
import com.zzbboke.myblogapi.service.MyblogDictionayInfoService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;


import javax.annotation.Resource;
import java.util.List;

/**
 * 数据字典明细表(MyblogDictionayInfo)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service("myblogDictionayInfoService")
public class MyblogDictionayInfoServiceImpl extends AbstractCrudService<MyblogDictionayInfoVO, MyblogDictionayInfoEntity, MyblogDictionayInfoDTO,Long> implements MyblogDictionayInfoService {
   
}