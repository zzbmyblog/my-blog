package com.zzbboke.myblogapi.service.impl;
import com.zzbboke.myblogapi.dto.MyblogAnnouncementDTO;
import com.zzbboke.myblogapi.entity.MyblogAnnouncementEntity;
import com.zzbboke.myblogapi.service.MyblogAnnouncementService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;
import com.zzbboke.myblogapi.vo.MyblogAnnouncementVO;
import org.springframework.stereotype.Service;

/**
 * 公告(MyblogAnnouncement)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service("myblogAnnouncementService")
public class MyblogAnnouncementServiceImpl extends AbstractCrudService<MyblogAnnouncementVO, MyblogAnnouncementEntity, MyblogAnnouncementDTO,Long> implements MyblogAnnouncementService {
   
}