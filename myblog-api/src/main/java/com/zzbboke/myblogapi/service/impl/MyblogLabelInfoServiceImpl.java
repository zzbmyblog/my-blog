package com.zzbboke.myblogapi.service.impl;
import org.springframework.stereotype.Service;

import com.zzbboke.myblogapi.entity.MyblogLabelInfoEntity;
import com.zzbboke.myblogapi.vo.MyblogLabelInfoVO;
import com.zzbboke.myblogapi.dto.MyblogLabelInfoDTO;
import com.zzbboke.myblogapi.service.MyblogLabelInfoService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;


import javax.annotation.Resource;
import java.util.List;

/**
 * 文章标签(MyblogLabelInfo)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service("myblogLabelInfoService")
public class MyblogLabelInfoServiceImpl extends AbstractCrudService<MyblogLabelInfoVO, MyblogLabelInfoEntity, MyblogLabelInfoDTO,Long> implements MyblogLabelInfoService {
   
}