package com.zzbboke.myblogapi.service.base;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 转换dto
 * 
 * @author zzb19
 */
public class ConversionServiceImpl<DOMAIN, VO, DTO> implements ConversionService<DOMAIN, VO, DTO> {

	/**
	 * 获取子类泛型的类型
	 * 
	 * @return Class<VO>
	 */

	@Override
	@SuppressWarnings("unchecked")
	public Class<VO> getVoGenericClass() {
		return (Class<VO>) getParameterizedType().getActualTypeArguments()[0];
	}

	@Override
	@SuppressWarnings("unchecked")
	public Class<DTO> getDtoGenericClass() {
		return (Class<DTO>) getParameterizedType().getActualTypeArguments()[0];
	}

	@Override
	@SuppressWarnings("unchecked")
	public Class<DOMAIN> getDomainGenericClass() {
		return (Class<DOMAIN>) getParameterizedType().getActualTypeArguments()[0];
	}

	public ParameterizedType getParameterizedType() {
		return (ParameterizedType) getClass().getGenericSuperclass();
	}

	/**
	 * @param domainList
	 * @return List VO
	 */

	@Override
	public List<VO> toListVo(Collection<DOMAIN> domainList) {
		Assert.notNull(domainList, "domainList  is not null!");
		return domainList.stream().map(this::toVo).collect(Collectors.toList());
	}

	/**
	 * @param domainPage
	 * @return Page VO
	 */
	@Override
	public Page<VO> toPageVo(Page<DOMAIN> domainPage) {
		Assert.notNull(domainPage, "domainPage  is not null!");
		return new PageImpl<>(this.toListVo(domainPage.getContent()));
	}

	@Override
	public VO toVo(DOMAIN domain) {
		Assert.notNull(domain, "DOMAIN  is not null!");
		final VO vo = BeanUtil.toBean(domain, this.getVoGenericClass());
		customizeToVO(domain, vo);
		return vo;
	}

	@Override
	public DTO toDto(VO vo) {
		Assert.notNull(vo, "vo  is not null!");
		final DTO dto = BeanUtil.toBean(vo, this.getDtoGenericClass());
		customizeVOToDTO(vo, dto);
		return dto;
	}

	@Override
	public DOMAIN toDomain(DTO dto) {
		Assert.notNull(dto, "DOMAIN  is not null!");
		final DOMAIN domain = BeanUtil.toBean(dto, this.getDomainGenericClass());
		customizeToDOMAIN(dto, domain);
		return domain;
	}

	@Override
	public Optional<VO> toOptionalVo(Optional<DOMAIN> domain) {
		Assert.notNull(domain, "DOMAIN  is not null!");
		return Optional.of(BeanUtil.toBean(domain, this.getVoGenericClass()));
	}

	@Override
	public void customizeToVO(DOMAIN domain, VO vo) {

	}

	@Override
	public void customizeToDTO(DOMAIN domain, DTO vo) {

	}

	public void customizeVOToDTO(VO vo, DTO dto) {

	}

	@Override
	public void customizeToDOMAIN(DTO dto, DOMAIN domain) {

	}
}
