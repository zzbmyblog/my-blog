package com.zzbboke.myblogapi.service;


import com.zzbboke.myblogapi.dto.MyblogResourcesDTO;
import com.zzbboke.myblogapi.entity.MyblogResourcesEntity;
import com.zzbboke.myblogapi.service.base.CrudService;
import com.zzbboke.myblogapi.vo.MyblogResourcesVO;

import java.util.Set;

/**
 * (MyblogResources)表服务接口
 *
 * @author bin.zong
 * @since 2021-06-27 08:53:47
 */
public interface MyblogResourcesService extends CrudService<MyblogResourcesVO, MyblogResourcesEntity, MyblogResourcesDTO,Long> {

    public Set<String> menuList();

}