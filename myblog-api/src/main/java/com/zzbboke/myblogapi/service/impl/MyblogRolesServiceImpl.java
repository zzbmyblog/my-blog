package com.zzbboke.myblogapi.service.impl;
import com.zzbboke.myblogapi.dto.MyblogRolesDTO;
import com.zzbboke.myblogapi.entity.MyblogRolesEntity;
import com.zzbboke.myblogapi.service.MyblogRolesService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;
import com.zzbboke.myblogapi.vo.MyblogRolesVO;
import org.springframework.stereotype.Service;

/**
 * 角色信息(MyblogRoles)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service("myblogRolesService")
public class MyblogRolesServiceImpl extends AbstractCrudService<MyblogRolesVO, MyblogRolesEntity, MyblogRolesDTO,Long> implements MyblogRolesService {

}