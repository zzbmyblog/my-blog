package com.zzbboke.myblogapi.service.impl;
import org.springframework.stereotype.Service;

import com.zzbboke.myblogapi.entity.MyblogLabelEntity;
import com.zzbboke.myblogapi.vo.MyblogLabelVO;
import com.zzbboke.myblogapi.dto.MyblogLabelDTO;
import com.zzbboke.myblogapi.service.MyblogLabelService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;


import javax.annotation.Resource;
import java.util.List;

/**
 * 文章标签中间表(MyblogLabel)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service("myblogLabelService")
public class MyblogLabelServiceImpl extends AbstractCrudService<MyblogLabelVO, MyblogLabelEntity, MyblogLabelDTO,Long> implements MyblogLabelService {
   
}