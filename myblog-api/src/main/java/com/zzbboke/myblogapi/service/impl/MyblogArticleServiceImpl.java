package com.zzbboke.myblogapi.service.impl;
import org.springframework.stereotype.Service;

import com.zzbboke.myblogapi.entity.MyblogArticleEntity;
import com.zzbboke.myblogapi.vo.MyblogArticleVO;
import com.zzbboke.myblogapi.dto.MyblogArticleDTO;
import com.zzbboke.myblogapi.service.MyblogArticleService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;


import javax.annotation.Resource;
import java.util.List;

/**
 * 文章信息(MyblogArticle)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service("myblogArticleService")
public class MyblogArticleServiceImpl extends AbstractCrudService<MyblogArticleVO, MyblogArticleEntity, MyblogArticleDTO,Long> implements MyblogArticleService {
   
}