package com.zzbboke.myblogapi.service.base;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.lang.NonNull;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author zongBin
 */
public interface CrudService<VO, DOMAIN,DTO, ID extends Serializable>{

    @NonNull
    List<VO> listAll();

    @NonNull
    List<VO> listAll(@NonNull Sort sort);

    @NonNull
    List<VO> listAll(@NonNull Predicate predicate);

    @NonNull
    Page<VO> listAll(@NonNull Pageable pageable);

    @NonNull
    Page<VO> listAll(@NonNull Sort sort, @NonNull Pageable pageable);

    @NonNull
    Page<VO> listAll(@NonNull Pageable pageable, @NonNull Predicate predicate);

    @NonNull
    List<VO> listAllByIds(@NonNull Collection<ID> ids);

    @NonNull
    List<VO> listAllByIds(@NonNull Collection<ID> ids, @NonNull Sort sort);

    @NonNull
    Optional<VO> fetchById(@NonNull ID id);

    @NonNull
    VO getById(@NonNull ID id);

    @NonNull
    boolean existsById(@NonNull ID id);

    @NonNull
    @Transactional
    VO create(@NonNull DOMAIN domain);

    @NonNull
    @Transactional
    List<VO> createAll(@NonNull Collection<DOMAIN> domains);

    @NonNull
    @Transactional
    VO update(@NonNull DOMAIN domain);

    @NonNull
    @Transactional
    List<VO> updateAll(@NonNull Collection<DOMAIN> domains);

    @Transactional
    void removeByDomain(@NonNull DOMAIN domain);

    @NonNull
    @Transactional
    void removeByIds(@NonNull Collection<ID> ids);

    @NonNull
    @Transactional
    void removeByIds(@NonNull ID id);

    List<VO> findAll(@NonNull Predicate predicate);

    List<VO> findAll(@NonNull Predicate predicate, @NonNull Pageable pageable);

}
