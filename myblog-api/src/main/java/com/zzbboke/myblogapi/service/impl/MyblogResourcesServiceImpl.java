package com.zzbboke.myblogapi.service.impl;
import com.zzbboke.myblogapi.dto.MyblogResourcesDTO;
import com.zzbboke.myblogapi.entity.MyblogResourcesEntity;
import com.zzbboke.myblogapi.service.MyblogResourcesService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;
import com.zzbboke.myblogapi.vo.MyblogResourcesVO;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * (MyblogResources)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-27 08:53:47
 */
@Service
public class MyblogResourcesServiceImpl extends AbstractCrudService<MyblogResourcesVO, MyblogResourcesEntity, MyblogResourcesDTO,Long> implements MyblogResourcesService {

    @Override
    public Set<String> menuList() {
        return null;
    }
}