package com.zzbboke.myblogapi.service.impl;
import org.springframework.stereotype.Service;

import com.zzbboke.myblogapi.entity.MyblogSystemEntity;
import com.zzbboke.myblogapi.vo.MyblogSystemVO;
import com.zzbboke.myblogapi.dto.MyblogSystemDTO;
import com.zzbboke.myblogapi.service.MyblogSystemService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;


import javax.annotation.Resource;
import java.util.List;

/**
 * 系统设置(MyblogSystem)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service("myblogSystemService")
public class MyblogSystemServiceImpl extends AbstractCrudService<MyblogSystemVO, MyblogSystemEntity, MyblogSystemDTO,Long> implements MyblogSystemService {
   
}