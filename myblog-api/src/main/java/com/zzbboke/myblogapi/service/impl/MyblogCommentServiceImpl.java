package com.zzbboke.myblogapi.service.impl;
import org.springframework.stereotype.Service;

import com.zzbboke.myblogapi.entity.MyblogCommentEntity;
import com.zzbboke.myblogapi.vo.MyblogCommentVO;
import com.zzbboke.myblogapi.dto.MyblogCommentDTO;
import com.zzbboke.myblogapi.service.MyblogCommentService;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;


import javax.annotation.Resource;
import java.util.List;

/**
 * 文章评论(MyblogComment)表服务实现类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Service("myblogCommentService")
public class MyblogCommentServiceImpl extends AbstractCrudService<MyblogCommentVO, MyblogCommentEntity, MyblogCommentDTO,Long> implements MyblogCommentService {
   
}