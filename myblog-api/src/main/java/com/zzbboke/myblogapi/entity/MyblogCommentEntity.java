package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 文章评论(MyblogComment)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "myblog_comment")
public class MyblogCommentEntity extends BaseEntity {

    private static final long serialVersionUID = 908837442285297982L;

     //评论内容
    private String content;

     //文章ID
    private Integer articeId;

     //上级ID
    private Integer parrentId;

     //用户ID
    private Integer userId;



}