package com.zzbboke.myblogapi.entity;

import com.google.common.base.Objects;

import javax.persistence.*;
import java.util.Set;

/**
 * 角色信息(MyblogRoles)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Table(name = "myblog_roles")
public class MyblogRolesEntity extends BaseEntity {

    private static final long serialVersionUID = -45574929232395461L;

    //角色名字0用户1是管理员2违规
    private String rname;

    private String description;

    @ManyToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JoinTable(name = "myblog_role_resources", joinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "RESOURCES_ID", referencedColumnName = "ID"))
    private Set<MyblogResourcesEntity> myblogResourcesEntities;


    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<MyblogResourcesEntity> getMyblogResourcesEntities() {
        return myblogResourcesEntities;
    }

    public void setMyblogResourcesEntities(Set<MyblogResourcesEntity> myblogResourcesEntities) {
        this.myblogResourcesEntities = myblogResourcesEntities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyblogRolesEntity that = (MyblogRolesEntity) o;
        return Objects.equal(rname, that.rname) &&
                Objects.equal(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(rname, description);
    }

    @Override
    public String toString() {
        return "MyblogRolesEntity{" +
                "rname='" + rname + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}