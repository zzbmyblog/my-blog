package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author zongBin
 */
@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = 4097397251492083113L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "bigint")
	private Long id;

	private LocalDateTime deleteTime;

	@LastModifiedDate
	private LocalDateTime updateTime;

	@CreatedDate
	private LocalDateTime createTime;

	@Override
	public String toString() {
		return "BaseEntity{" + "deleteTime=" + deleteTime + ", updateTime=" + updateTime + ", createTime=" + createTime
				+ '}';
	}
}
