package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 联系方式(MyblogContact)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "myblog_contact")
public class MyblogContactEntity extends BaseEntity {

    private static final long serialVersionUID = 251015727034363248L;

     //QQ
    private String qq;

     //邮箱
    private String email;

     //m码云
    private String gitee;

     //GitHub
    private String github;

     //微博
    private String weibo;



}