package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 文章信息(MyblogArticle)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "myblog_article")
public class MyblogArticleEntity extends BaseEntity {

    private static final long serialVersionUID = -63797911960437296L;

     //文章标题
    private String title;

     //文章副标题
    private String subtitle;

     //文章内容
    private String content;

     //封面图片
    private String coverImg;

     //关联用户
    private Integer userId;

     //文章游览量
    private Integer pageview;



}