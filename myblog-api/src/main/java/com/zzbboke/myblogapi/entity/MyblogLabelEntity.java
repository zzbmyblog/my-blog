package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 文章标签中间表(MyblogLabel)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "myblog_label")
public class MyblogLabelEntity extends BaseEntity {

    private static final long serialVersionUID = -63735836794167437L;

     //文章ID
    private Integer articleId;

     //标签ID
    private Integer labelInfoId;


}