package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 公告(MyblogAnnouncement)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "myblog_announcement")
public class MyblogAnnouncementEntity extends BaseEntity {

    private static final long serialVersionUID = -40136199452406197L;

     //主键ID
    private Long id;

     //公告内容
    private String connect;


}