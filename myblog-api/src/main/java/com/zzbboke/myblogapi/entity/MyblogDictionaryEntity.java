package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 数据字典分类表(MyblogDictionary)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "myblog_dictionary")
public class MyblogDictionaryEntity extends BaseEntity {

    private static final long serialVersionUID = 779907303462038366L;



}