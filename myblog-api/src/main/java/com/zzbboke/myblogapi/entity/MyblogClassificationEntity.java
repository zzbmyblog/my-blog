package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 菜单分类表(MyblogClassification)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "myblog_classification")
public class MyblogClassificationEntity extends BaseEntity {

    private static final long serialVersionUID = -56263127925714049L;

     //父类别id当id=0时说明是根节点,一级类别
    private Integer parentId;

     //类别名称
    private String name;

     //排序顺序
    private Integer index;



}