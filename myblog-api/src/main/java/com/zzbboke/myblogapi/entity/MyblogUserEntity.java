package com.zzbboke.myblogapi.entity;

import cn.hutool.core.collection.CollectionUtil;
import com.zzbboke.myblogapi.exception.NotFoundDataException;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

/**
 * 用户信息(MyblogUser)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Table(name = "myblog_user")
@DynamicInsert
@DynamicUpdate
public class MyblogUserEntity extends BaseEntity implements UserDetails {

    private static final long serialVersionUID = -60263102819050981L;

    //用户账号
    private String username;

    //用户密码
    private String password;

    //邮箱地址
    private String emailAddress;


    //找回密码凭证
    private String activationCode;


    @ManyToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JoinTable(name = "myblog_user_roles", joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"))
    private Set<MyblogRolesEntity> myblogRoles;


    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorizationInformationList = new LinkedList<>();
        if (CollectionUtil.isEmpty(this.getMyblogRoles())) {
            return Collections.emptyList();
        }
        for (MyblogRolesEntity myblogRolesEntity : this.getMyblogRoles()) {
            Optional.ofNullable(myblogRolesEntity.getRname()).orElseThrow(() -> new NotFoundDataException(username + ":授权信息为null"));
            SimpleGrantedAuthority simpleGrantedAuthority=new SimpleGrantedAuthority(myblogRolesEntity.getRname());
            authorizationInformationList.add(simpleGrantedAuthority);
        }
        return authorizationInformationList;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Set<MyblogRolesEntity> getMyblogRoles() {
        return myblogRoles;
    }

    public void setMyblogRoles(Set<MyblogRolesEntity> myblogRoles) {
        this.myblogRoles = myblogRoles;
    }

}