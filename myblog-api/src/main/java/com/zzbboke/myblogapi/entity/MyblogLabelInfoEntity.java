package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 文章标签(MyblogLabelInfo)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "myblog_label_info")
public class MyblogLabelInfoEntity extends BaseEntity {

    private static final long serialVersionUID = -37657807858713147L;

     //标签内容
    private String connent;



}