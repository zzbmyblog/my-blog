package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * (MyblogResources)实体类
 *
 * @author bin.zong
 * @since 2021-06-27 08:53:47
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "myblog_resources")
public class MyblogResourcesEntity extends BaseEntity {

    private static final long serialVersionUID = -51548858537424023L;

     //资源路径
    private String url;

     //资源描述
    private String urlDescription;


    private String resourcesCode;

    private Long parentId;



}