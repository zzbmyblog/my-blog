package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 系统设置(MyblogSystem)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "myblog_system")
public class MyblogSystemEntity extends BaseEntity {

    private static final long serialVersionUID = 480966084514388436L;

     //用户游览量
    private Integer number;


     //备案信息
    private String record;

     //META
    private String metaConnect;

     //设置标题
    private String title;



}