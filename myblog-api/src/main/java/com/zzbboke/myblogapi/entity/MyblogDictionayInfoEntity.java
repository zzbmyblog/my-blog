package com.zzbboke.myblogapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 数据字典明细表(MyblogDictionayInfo)实体类
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Entity
@Getter
@Setter
@ToString
@Table(name = "myblog_dictionay_info")
public class MyblogDictionayInfoEntity extends BaseEntity {

    private static final long serialVersionUID = -99145419137784706L;

     //字典值
    private String connect;



}