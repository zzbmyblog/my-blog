package com.zzbboke.myblogapi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
public class MyblogSystemDTO extends BaseDTO {

    private static final long serialVersionUID = -79097341070039243L;

     //用户游览量
    private Integer number;

     //创建时间
    private LocalDateTime createdTime;

     //备案信息
    private String record;

     //META
    private String metaConnect;

     //设置标题
    private String title;


}