package com.zzbboke.myblogapi.dto;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NotNull(message = "邮箱或用户名必填一个")
public class UsernameOrEmailDTO {

    private String username;

    private String emailAddress;

    public UsernameOrEmailDTO() {
    }

    public UsernameOrEmailDTO(String username, String emailAddress) {
        this.username = username;
        this.emailAddress = emailAddress;
    }
}
