package com.zzbboke.myblogapi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogLabelDTO extends BaseDTO {

    private static final long serialVersionUID = -55973964545104647L;

     //文章ID
    private Integer articleId;

     //标签ID
    private Integer labelInfoId;


}