package com.zzbboke.myblogapi.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.validation.constraints.*;
import java.time.LocalDateTime;

/**
 * @author zongBin
 */
@Getter
@Setter
public class BaseDTO {

	public interface insert {};

	public interface update {};

	@NotNull(message = "更新id不能为null",groups = update.class)
	private Long id;

	private LocalDateTime deleteTime;

	@LastModifiedDate
	private LocalDateTime updateTime;

	@CreatedDate
	private LocalDateTime createTime;

	@Override
	public String toString() {
		return "BaseDTO{" + "deleteTime=" + deleteTime + ", updateTime=" + updateTime + ", createTime=" + createTime
				+ '}';
	}
}
