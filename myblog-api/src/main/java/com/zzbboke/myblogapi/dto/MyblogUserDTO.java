package com.zzbboke.myblogapi.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class MyblogUserDTO extends BaseDTO {

    private static final long serialVersionUID = -14843914745959952L;

    //用户账号
    @NotNull(message = "用户名称不能为null", groups = insert.class)
    private String username;

    //用户密码
    @NotNull(message = "用户密码不能为null", groups = insert.class)
    private String password;

    //邮箱地址
    @NotNull(message = "用户邮箱不能为null", groups = insert.class)
    private String emailAddress;

    //找回密码凭证
    private String activationCode;


}