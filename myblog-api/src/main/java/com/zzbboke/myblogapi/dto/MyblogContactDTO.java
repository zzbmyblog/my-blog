package com.zzbboke.myblogapi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogContactDTO extends BaseDTO {

    private static final long serialVersionUID = -88422011338460488L;

     //QQ
    private String qq;

     //邮箱
    private String email;

     //m码云
    private String gitee;

     //GitHub
    private String github;

     //微博
    private String weibo;




}