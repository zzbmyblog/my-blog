package com.zzbboke.myblogapi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogAnnouncementDTO extends BaseDTO {

    private static final long serialVersionUID = -48602342693946767L;

     //主键ID
    private Long id;

     //公告内容
    private String connect;

}