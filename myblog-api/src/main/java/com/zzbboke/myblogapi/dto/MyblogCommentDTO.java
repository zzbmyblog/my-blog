package com.zzbboke.myblogapi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogCommentDTO extends BaseDTO {

    private static final long serialVersionUID = -42625046984152511L;

     //评论内容
    private String content;

     //文章ID
    private Integer articeId;

     //上级ID
    private Integer parrentId;

     //用户ID
    private Integer userId;



}