package com.zzbboke.myblogapi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogRoleResourcesDTO extends BaseDTO {

    private static final long serialVersionUID = -99787836417290980L;

     //权限id
    private Long roleId;

     //资源id
    private Long resourcesId;


}