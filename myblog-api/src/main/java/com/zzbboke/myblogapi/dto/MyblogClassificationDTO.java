package com.zzbboke.myblogapi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogClassificationDTO extends BaseDTO {

    private static final long serialVersionUID = -18946535152086674L;

     //父类别id当id=0时说明是根节点,一级类别
    private Integer parentId;

     //类别名称
    private String name;

     //排序顺序
    private Integer index;


}