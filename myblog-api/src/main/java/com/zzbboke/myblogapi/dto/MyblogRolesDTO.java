package com.zzbboke.myblogapi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogRolesDTO extends BaseDTO {

    private static final long serialVersionUID = 593232413952592229L;

     //角色名字0用户1是管理员2违规
    private String rname;

    private String description;

}