package com.zzbboke.myblogapi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogUserRoleDTO extends BaseDTO {

    private static final long serialVersionUID = 773935927606706689L;

     //用户id
    private Long userId;

     //角色id
    private Long roleId;


}