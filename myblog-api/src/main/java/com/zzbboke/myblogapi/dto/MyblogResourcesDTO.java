package com.zzbboke.myblogapi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogResourcesDTO extends BaseDTO {

    private static final long serialVersionUID = -51027922514363186L;

     //资源路径
    private String url;

     //资源描述
    private String urlDescription;


}