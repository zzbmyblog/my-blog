package com.zzbboke.myblogapi.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogArticleVO extends BaseVO {

    private static final long serialVersionUID = 234824116034406536L;

     //文章标题
    private String title;

     //文章副标题
    private String subtitle;

     //文章内容
    private String content;

     //封面图片
    private String coverImg;

     //关联用户
    private Integer userId;

     //文章游览量
    private Integer pageview;


}