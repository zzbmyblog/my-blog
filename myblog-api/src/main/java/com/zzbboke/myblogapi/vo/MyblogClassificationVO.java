package com.zzbboke.myblogapi.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogClassificationVO extends BaseVO {

    private static final long serialVersionUID = -59217230280056116L;

     //父类别id当id=0时说明是根节点,一级类别
    private Integer parentId;

     //类别名称
    private String name;

     //排序顺序
    private Integer index;



}