package com.zzbboke.myblogapi.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogLabelInfoVO extends BaseVO {

    private static final long serialVersionUID = 932947332309433923L;

     //主键ID
    private Integer id;

     //标签内容
    private String connent;


}