package com.zzbboke.myblogapi.vo;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class MyblogResourcesVO extends BaseVO {

    private static final long serialVersionUID = -36655842043067393L;

    //资源路径
    private String url;

    //资源描述
    private String urlDescription;


    private String resourcesCode;

    private Long parentId;



}