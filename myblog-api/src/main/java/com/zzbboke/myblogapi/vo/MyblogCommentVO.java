package com.zzbboke.myblogapi.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogCommentVO extends BaseVO {

    private static final long serialVersionUID = 564650339400514501L;

     //评论内容
    private String content;

     //文章ID
    private Integer articeId;

     //上级ID
    private Integer parrentId;

     //用户ID
    private Integer userId;



}