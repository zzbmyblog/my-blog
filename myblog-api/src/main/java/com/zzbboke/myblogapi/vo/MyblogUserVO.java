package com.zzbboke.myblogapi.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class MyblogUserVO extends BaseVO {

    private static final long serialVersionUID = 888878760640850408L;

    //用户账号
    private String username;

    //用户密码
    @JsonIgnore
    private String password;

    //邮箱地址
    private String emailAddress;

    //找回密码凭证
    private String activationCode;

    private Set<MyblogRolesVO> myblogRoles;

    //菜单列表
    private List<HashMap<String,String>> menuList;




    @Override
    public String toString() {
        return "MyblogUserVO{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}