package com.zzbboke.myblogapi.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogContactVO extends BaseVO {

    private static final long serialVersionUID = -28630046213607716L;

     //QQ
    private String qq;

     //邮箱
    private String email;

     //m码云
    private String gitee;

     //GitHub
    private String github;

     //微博
    private String weibo;



}