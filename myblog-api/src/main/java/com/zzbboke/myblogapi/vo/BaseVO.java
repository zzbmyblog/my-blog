package com.zzbboke.myblogapi.vo;

import java.time.LocalDateTime;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zongBin
 */
@Getter
@Setter
public class BaseVO {

	private LocalDateTime deleteTime;

	@LastModifiedDate
	private LocalDateTime updateTime;

	@CreatedDate
	private LocalDateTime createTime;

	@Override
	public String toString() {
		return "BaseVO{" +
			"deleteTime=" + deleteTime +
			", updateTime=" + updateTime +
			", createTime=" + createTime +
			'}';
	}
}
