package com.zzbboke.myblogapi.vo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zzbboke.myblogapi.constant.ResponseCode;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import springfox.documentation.spring.web.json.Json;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.UiConfiguration;

import java.util.ArrayList;

@Slf4j
@RestControllerAdvice(basePackages = "com.zzbboke.myblogapi.web.controller")
public class ResponseAdvice implements ResponseBodyAdvice {

    private static String[] ignores = new String[]{
            //过滤swagger相关的请求的接口，不然swagger会提示base-url被拦截
            "/swagger-ui.html",
            "/swagger-ui/",
            "/swagger-resources/",
            "/v2/api-docs",
            "/v3/api-docs",
            "/webjars/",
    };

    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true;
    }

    @SneakyThrows
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        if (body instanceof Response) {
            return body;
        }


        if (body instanceof String) {
            ObjectMapper objectMapper = new ObjectMapper();
            String s = objectMapper.writeValueAsString(body);
            return s;
        }
        //放行swagger
        if (body instanceof Json || body instanceof UiConfiguration || body instanceof ArrayList && ((ArrayList) body).get(0) instanceof SwaggerResource)
            return body;


        log.info("returnType={}", returnType);


        return new Response<>(ResponseCode.SUCCESS, body);
    }


}
