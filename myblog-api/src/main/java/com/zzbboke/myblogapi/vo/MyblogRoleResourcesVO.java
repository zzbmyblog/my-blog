package com.zzbboke.myblogapi.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@ToString
public class MyblogRoleResourcesVO extends BaseVO {

    private static final long serialVersionUID = -99820825745095902L;

     //权限id
    private Long roleId;

     //资源id
    private Long resourcesId;



}