package com.zzbboke.myblogapi.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class MyblogRolesVO extends BaseVO {

    private static final long serialVersionUID = -47096306679195723L;

     //角色名字0用户1是管理员2违规
    private String rname;

    private String description;

    private Set<MyblogResourcesVO> myblogResourcesEntities;


    @Override
    public String toString() {
        return "MyblogRolesVO{" +
                "rname='" + rname + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}