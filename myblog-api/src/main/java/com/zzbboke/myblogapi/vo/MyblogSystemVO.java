package com.zzbboke.myblogapi.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogSystemVO extends BaseVO {

    private static final long serialVersionUID = 901231099283875964L;

     //用户游览量
    private Integer number;


     //备案信息
    private String record;

     //META
    private String metaConnect;

     //设置标题
    private String title;



}