package com.zzbboke.myblogapi.vo;

import cn.hutool.core.lang.Assert;
import com.zzbboke.myblogapi.constant.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zongBin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response<T> implements Serializable {

    private static final long serialVersionUID = -699518333000737665L;

    private Integer code;

    private String message;

    private T data;

    public Response(ResponseCode responseCode, T data) {
        this.code = responseCode.getCode();
        this.message = responseCode.getDesc();
        this.data = data;
    }


    public static <T> Response<T> from(ResponseCode responseCode, T data) {
        Assert.notNull(data, "data not null");
        return new Response<T>(responseCode, data);
    }


}
