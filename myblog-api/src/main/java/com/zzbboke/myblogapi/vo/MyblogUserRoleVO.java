package com.zzbboke.myblogapi.vo;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class MyblogUserRoleVO extends BaseVO {

    private static final long serialVersionUID = -69968919113601254L;

     //用户id
    private Long userId;

     //角色id
    private Long roleId;


}