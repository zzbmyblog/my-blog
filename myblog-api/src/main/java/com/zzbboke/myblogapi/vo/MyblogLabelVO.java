package com.zzbboke.myblogapi.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MyblogLabelVO extends BaseVO {

    private static final long serialVersionUID = -34553650078344000L;

     //文章ID
    private Integer articleId;

     //标签ID
    private Integer labelInfoId;



}