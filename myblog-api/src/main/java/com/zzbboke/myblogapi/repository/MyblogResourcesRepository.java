package com.zzbboke.myblogapi.repository;

import com.zzbboke.myblogapi.entity.MyblogResourcesEntity;
import com.zzbboke.myblogapi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * (MyblogResources)表数据库访问层
 *
 * @author bin.zong
 * @since 2021-06-27 08:53:47
 */
 @Repository
public interface MyblogResourcesRepository extends BaseRepository<MyblogResourcesEntity, Long> {
}