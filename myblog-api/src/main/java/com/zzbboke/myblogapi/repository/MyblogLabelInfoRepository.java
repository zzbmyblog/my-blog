package com.zzbboke.myblogapi.repository;

import com.zzbboke.myblogapi.entity.MyblogLabelInfoEntity;
import com.zzbboke.myblogapi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * 文章标签(MyblogLabelInfo)表数据库访问层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
 @Repository
public interface MyblogLabelInfoRepository extends BaseRepository<MyblogLabelInfoEntity, Long> {
}