package com.zzbboke.myblogapi.repository;

import com.zzbboke.myblogapi.entity.MyblogArticleEntity;
import com.zzbboke.myblogapi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * 文章信息(MyblogArticle)表数据库访问层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
 @Repository
public interface MyblogArticleRepository extends BaseRepository<MyblogArticleEntity, Long> {
}