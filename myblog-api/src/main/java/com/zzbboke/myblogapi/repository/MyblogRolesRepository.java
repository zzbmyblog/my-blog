package com.zzbboke.myblogapi.repository;

import com.zzbboke.myblogapi.entity.MyblogRolesEntity;
import com.zzbboke.myblogapi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * 角色信息(MyblogRoles)表数据库访问层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
 @Repository
public interface MyblogRolesRepository extends BaseRepository<MyblogRolesEntity, Long> {
}