package com.zzbboke.myblogapi.repository.base;

import org.springframework.data.domain.Sort;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author zongBin
 */

public abstract class AbstractBaseRepositories<DOMAIN, ID> implements BaseRepository<DOMAIN, ID> {



    @Override
    public List<DOMAIN> findAllByIdIn(Collection ids, Sort sort) {
        if (!ids.iterator().hasNext()) {
            return Collections.emptyList();
        }
        return this.findAllById(ids);
    }
}
