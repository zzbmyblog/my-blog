package com.zzbboke.myblogapi.repository;

import com.zzbboke.myblogapi.entity.MyblogClassificationEntity;
import com.zzbboke.myblogapi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * 菜单分类表(MyblogClassification)表数据库访问层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
 @Repository
public interface MyblogClassificationRepository extends BaseRepository<MyblogClassificationEntity, Long> {
}