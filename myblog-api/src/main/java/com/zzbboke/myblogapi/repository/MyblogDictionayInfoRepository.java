package com.zzbboke.myblogapi.repository;

import com.zzbboke.myblogapi.entity.MyblogDictionayInfoEntity;
import com.zzbboke.myblogapi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * 数据字典明细表(MyblogDictionayInfo)表数据库访问层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
 @Repository
public interface MyblogDictionayInfoRepository extends BaseRepository<MyblogDictionayInfoEntity, Long> {
}