package com.zzbboke.myblogapi.repository.base;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.Collection;
import java.util.List;

/**
 * Base repository interface contains some common methods.
 *
 * @param <DOMAIN> domain type
 * @param <ID>     id type
 * @author zongbin
 * @date 2021-03-15
 */
@NoRepositoryBean
public interface BaseRepository<DOMAIN, ID> extends JpaRepository<DOMAIN, ID>, PagingAndSortingRepository<DOMAIN, ID>,
        QueryByExampleExecutor<DOMAIN>,
        JpaSpecificationExecutor<DOMAIN>,
        QuerydslPredicateExecutor<DOMAIN> {


    public List<DOMAIN> findAllByIdIn(Collection ids, Sort sort);

}
