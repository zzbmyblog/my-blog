package com.zzbboke.myblogapi.repository;

import com.zzbboke.myblogapi.entity.MyblogUserEntity;
import com.zzbboke.myblogapi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * 用户信息(MyblogUser)表数据库访问层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@Repository
public interface MyblogUserRepository extends BaseRepository<MyblogUserEntity, Long> {

    /**
     * @param username
     * @Description 根据用户名查询用户
     * @Return
     */
    Optional<MyblogUserEntity> findByUsername(String username);

    /**
     * @param password
     * @Description 根据密码查询用户
     * @Return
     */
    Optional<MyblogUserEntity> findByPassword(String password);

    /**
     * @param emailaddress
     * @Description 根据邮箱查询用户
     * @Return
     */
    Optional<MyblogUserEntity> findByEmailAddress(String emailaddress);

    Optional<MyblogUserEntity> findByActivationCode(String activationCode);
}