package com.zzbboke.myblogapi.repository;

import com.zzbboke.myblogapi.entity.MyblogLabelEntity;
import com.zzbboke.myblogapi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * 文章标签中间表(MyblogLabel)表数据库访问层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
 @Repository
public interface MyblogLabelRepository extends BaseRepository<MyblogLabelEntity, Long> {
}