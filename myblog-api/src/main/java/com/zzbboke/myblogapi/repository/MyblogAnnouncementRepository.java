package com.zzbboke.myblogapi.repository;

import com.zzbboke.myblogapi.entity.MyblogAnnouncementEntity;
import com.zzbboke.myblogapi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * 公告(MyblogAnnouncement)表数据库访问层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
 @Repository
public interface MyblogAnnouncementRepository extends BaseRepository<MyblogAnnouncementEntity, Long> {
}