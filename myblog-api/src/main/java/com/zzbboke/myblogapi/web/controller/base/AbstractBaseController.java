package com.zzbboke.myblogapi.web.controller.base;

import com.querydsl.core.types.Predicate;
import com.zzbboke.myblogapi.dto.BaseDTO;
import com.zzbboke.myblogapi.service.base.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

/**
 * @author zongBin
 */
public abstract class AbstractBaseController<VO, DOMAIN, DTO, ID extends Serializable> {
	@Autowired
	private AbstractCrudService<VO, DOMAIN, DTO, ID> abstractCrudService;


	@GetMapping("/{id}")
	public VO findById(@PathVariable(name = "id") ID id) {
		return abstractCrudService.fetchById(id).orElse(null);
	}

	@GetMapping
	public abstract Object list(Pageable pageable, Sort sort, Predicate predicate);

	protected Object doList(Pageable pageable, Sort sort, Predicate predicate) {
		if (null != predicate) {
			if (null != pageable && pageable.getPageSize() != Integer.MAX_VALUE) {
				return abstractCrudService.listAll(pageable, predicate);
			}

			return abstractCrudService.listAll(predicate);
		}

		if (null != pageable && pageable.getPageSize() != Integer.MAX_VALUE) {
			return abstractCrudService.listAll(pageable);
		}

		if (null != sort) {
			return abstractCrudService.listAll(sort);
		}

		return abstractCrudService.listAll();
	}

	@PutMapping
	public VO create(@RequestBody @Validated(value = BaseDTO.insert.class) DTO dto) {
		checkDto(dto);
		return abstractCrudService.create(abstractCrudService.toDomain(dto));
	}


	@PostMapping
	public VO update(@RequestBody @Validated(value = BaseDTO.update.class) DTO dto) {
		checkDto(dto);
		return abstractCrudService.create(abstractCrudService.toDomain(dto));
	}

	@DeleteMapping("/{id}")
	public boolean isDelete(@PathVariable ID id){
		return abstractCrudService.removeById(id);
	}

	public void checkDto(DTO dto) {
	}


}
