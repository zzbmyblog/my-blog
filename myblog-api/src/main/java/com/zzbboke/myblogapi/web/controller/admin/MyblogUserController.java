package com.zzbboke.myblogapi.web.controller.admin;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.querydsl.core.types.Predicate;
import com.zzbboke.myblogapi.web.controller.base.AbstractBaseController;
import com.zzbboke.myblogapi.entity.MyblogUserEntity;
import com.zzbboke.myblogapi.vo.MyblogUserVO;
import com.zzbboke.myblogapi.dto.MyblogUserDTO;

/**
 * 用户信息(MyblogUser)表控制层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@RestController
@RequestMapping("/admin/myblogUser")
public class MyblogUserController extends AbstractBaseController<MyblogUserVO, MyblogUserEntity, MyblogUserDTO,Long> {

	@Override
	public Object list(Pageable pageable, Sort sort, @QuerydslPredicate(root = MyblogUserEntity.class) Predicate predicate) {
		return super.doList(pageable, sort, predicate);
	}

	@GetMapping("/test")
	public String test(){
		return "测试";
	}

}