package com.zzbboke.myblogapi.web.controller.login;

import cn.hutool.core.util.StrUtil;
import com.zzbboke.myblogapi.service.MyblogUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RequestMapping("check")
@RestController
@Api(tags = "用户检查接口")
public class checkController {
    @Autowired
    private MyblogUserService myblogUserService;

    @GetMapping("/isExistUsername/{username}")
    @ApiOperation("根据用户名判断是否存在用户")
    public Boolean isExistUsername(@Validated @NotNull(message = "用户名称不能为null") String username) {
        if (StrUtil.hasEmpty(username)){
            return myblogUserService.isExistUserName(username);
        }
        return false;
    }

    @GetMapping("/isExistEmail/{email]")
    @ApiOperation("根据email判断是否存在用户")
    public Boolean isExistEmailAddress(@Validated @NotNull(message = "邮箱地址不能为null") String email) {
        if (StrUtil.hasEmpty(email)){
            return myblogUserService.isExistEmailAddress(email);
        }
        return false;
    }

    @GetMapping("/isExistActivationCode/{activationCode]")
    @ApiOperation("根据activationCode判断是否存在activationCode")
    public Boolean isExistActivationCode(@Validated @NotNull(message = "激活码不能为null") String activationCode) {
        if (StrUtil.hasEmpty(activationCode)){
            return myblogUserService.isExistEmailAddress(activationCode);
        }
        return false;
    }
}
