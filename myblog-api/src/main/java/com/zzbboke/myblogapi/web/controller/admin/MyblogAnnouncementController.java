package com.zzbboke.myblogapi.web.controller.admin;
import com.querydsl.core.types.Predicate;
import com.zzbboke.myblogapi.web.controller.base.AbstractBaseController;
import com.zzbboke.myblogapi.dto.MyblogAnnouncementDTO;
import com.zzbboke.myblogapi.entity.MyblogAnnouncementEntity;
import com.zzbboke.myblogapi.vo.MyblogAnnouncementVO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 公告(MyblogAnnouncement)表控制层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@RestController
@RequestMapping("admin/myblogAnnouncement")
public class MyblogAnnouncementController extends AbstractBaseController<MyblogAnnouncementVO, MyblogAnnouncementEntity, MyblogAnnouncementDTO,Long> {

	@Override
	public Object list(Pageable pageable, Sort sort, @QuerydslPredicate(root = MyblogAnnouncementEntity.class) Predicate predicate) {
		return super.doList(pageable, sort, predicate);
	}

}