package com.zzbboke.myblogapi.web.controller.admin;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.querydsl.core.types.Predicate;
import com.zzbboke.myblogapi.web.controller.base.AbstractBaseController;
import com.zzbboke.myblogapi.entity.MyblogArticleEntity;
import com.zzbboke.myblogapi.vo.MyblogArticleVO;
import com.zzbboke.myblogapi.dto.MyblogArticleDTO;

/**
 * 文章信息(MyblogArticle)表控制层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@RestController
@RequestMapping("myblogArticle")
public class MyblogArticleController extends AbstractBaseController<MyblogArticleVO, MyblogArticleEntity, MyblogArticleDTO,Long> {

	@Override
	public Object list(Pageable pageable, Sort sort, @QuerydslPredicate(root = MyblogArticleEntity.class) Predicate predicate) {
		return super.doList(pageable, sort, predicate);
	}

}