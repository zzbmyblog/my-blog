package com.zzbboke.myblogapi.web.controller.admin;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.querydsl.core.types.Predicate;
import com.zzbboke.myblogapi.web.controller.base.AbstractBaseController;
import com.zzbboke.myblogapi.entity.MyblogResourcesEntity;
import com.zzbboke.myblogapi.vo.MyblogResourcesVO;
import com.zzbboke.myblogapi.dto.MyblogResourcesDTO;

/**
 * (MyblogResources)表控制层
 *
 * @author bin.zong
 * @since 2021-06-27 08:53:47
 */
@RestController
@RequestMapping("myblogResources")
public class MyblogResourcesController extends AbstractBaseController<MyblogResourcesVO, MyblogResourcesEntity, MyblogResourcesDTO,Long> {

	@Override
	public Object list(Pageable pageable, Sort sort, @QuerydslPredicate(root = MyblogResourcesEntity.class) Predicate predicate) {
		return super.doList(pageable, sort, predicate);
	}

}