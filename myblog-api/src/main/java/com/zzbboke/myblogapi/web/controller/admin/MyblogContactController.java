package com.zzbboke.myblogapi.web.controller.admin;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.querydsl.core.types.Predicate;
import com.zzbboke.myblogapi.web.controller.base.AbstractBaseController;
import com.zzbboke.myblogapi.entity.MyblogContactEntity;
import com.zzbboke.myblogapi.vo.MyblogContactVO;
import com.zzbboke.myblogapi.dto.MyblogContactDTO;

/**
 * 联系方式(MyblogContact)表控制层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@RestController
@RequestMapping("myblogContact")
public class MyblogContactController extends AbstractBaseController<MyblogContactVO, MyblogContactEntity, MyblogContactDTO,Long> {

	@Override
	public Object list(Pageable pageable, Sort sort, @QuerydslPredicate(root = MyblogContactEntity.class) Predicate predicate) {
		return super.doList(pageable, sort, predicate);
	}

}