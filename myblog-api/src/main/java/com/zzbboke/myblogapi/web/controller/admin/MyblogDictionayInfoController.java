package com.zzbboke.myblogapi.web.controller.admin;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.querydsl.core.types.Predicate;
import com.zzbboke.myblogapi.web.controller.base.AbstractBaseController;
import com.zzbboke.myblogapi.entity.MyblogDictionayInfoEntity;
import com.zzbboke.myblogapi.vo.MyblogDictionayInfoVO;
import com.zzbboke.myblogapi.dto.MyblogDictionayInfoDTO;

/**
 * 数据字典明细表(MyblogDictionayInfo)表控制层
 *
 * @author bin.zong
 * @since 2021-06-24 21:34:42
 */
@RestController
@RequestMapping("myblogDictionayInfo")
public class MyblogDictionayInfoController extends AbstractBaseController<MyblogDictionayInfoVO, MyblogDictionayInfoEntity, MyblogDictionayInfoDTO,Long> {

	@Override
	public Object list(Pageable pageable, Sort sort, @QuerydslPredicate(root = MyblogDictionayInfoEntity.class) Predicate predicate) {
		return super.doList(pageable, sort, predicate);
	}

}