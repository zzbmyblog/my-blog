/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 8.0.16 : Database - myblog
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`myblog` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `myblog`;

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `xname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `parentid` int(11) DEFAULT '0',
  `parentid_list` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `depth` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `state` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1',
  `priority` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `categories` */

insert  into `categories`(`id`,`name`,`xname`,`parentid`,`parentid_list`,`depth`,`state`,`priority`) values 
(1,'图书',NULL,0,'1','1','1','0'),
(2,'科技',NULL,1,'1,2','2','1','0'),
(3,'计算机/互联网',NULL,2,'1,2,3','3','1','0'),
(4,'医学',NULL,2,'1,2,4','3','1','0');

/*Table structure for table `hibernate_sequence` */

DROP TABLE IF EXISTS `hibernate_sequence`;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `hibernate_sequence` */

insert  into `hibernate_sequence`(`next_val`) values 
(26);

/*Table structure for table `myblog_announcement` */

DROP TABLE IF EXISTS `myblog_announcement`;

CREATE TABLE `myblog_announcement` (
  `ID` bigint(20) DEFAULT NULL COMMENT '主键ID',
  `CONNECT` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公告内容',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='公告';

/*Data for the table `myblog_announcement` */

insert  into `myblog_announcement`(`ID`,`CONNECT`,`DELETE_TIME`,`UPDATE_TIME`,`CREATE_TIME`) values 
(1,'1',NULL,'2021-07-10 18:10:12','2021-07-10 18:10:01');

/*Table structure for table `myblog_article` */

DROP TABLE IF EXISTS `myblog_article`;

CREATE TABLE `myblog_article` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `TITLE` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文章标题',
  `SUBTITLE` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文章副标题',
  `CONTENT` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '文章内容',
  `COVER_IMG` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '封面图片',
  `USER_ID` int(11) DEFAULT NULL COMMENT '关联用户',
  `PAGEVIEW` int(11) DEFAULT NULL COMMENT '文章游览量',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATED_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文章信息';

/*Data for the table `myblog_article` */

/*Table structure for table `myblog_classification` */

DROP TABLE IF EXISTS `myblog_classification`;

CREATE TABLE `myblog_classification` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `PARENT_ID` int(11) DEFAULT NULL COMMENT '父类别id当id=0时说明是根节点,一级类别',
  `NAME` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类别名称',
  `INDEX` int(11) NOT NULL COMMENT '排序顺序',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATED_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单分类表';

/*Data for the table `myblog_classification` */

/*Table structure for table `myblog_comment` */

DROP TABLE IF EXISTS `myblog_comment`;

CREATE TABLE `myblog_comment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `CONTENT` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评论内容',
  `ARTICE_ID` int(11) NOT NULL COMMENT '文章ID',
  `PARRENT_ID` int(11) DEFAULT NULL COMMENT '上级ID',
  `USER_ID` int(11) DEFAULT NULL COMMENT '用户ID',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATED_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文章评论';

/*Data for the table `myblog_comment` */

/*Table structure for table `myblog_contact` */

DROP TABLE IF EXISTS `myblog_contact`;

CREATE TABLE `myblog_contact` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `QQ` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'QQ',
  `EMAIL` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `GITEE` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'm码云',
  `GITHUB` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'GitHub',
  `WEIBO` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微博',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATED_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='联系方式';

/*Data for the table `myblog_contact` */

/*Table structure for table `myblog_dictionary` */

DROP TABLE IF EXISTS `myblog_dictionary`;

CREATE TABLE `myblog_dictionary` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `DISCTIONARY_INFO_ID` int(11) NOT NULL COMMENT '关联字典明细表',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATED_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`,`DISCTIONARY_INFO_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='数据字典分类表';

/*Data for the table `myblog_dictionary` */

/*Table structure for table `myblog_dictionay_info` */

DROP TABLE IF EXISTS `myblog_dictionay_info`;

CREATE TABLE `myblog_dictionay_info` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `CONNECT` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '字典值',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATED_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='数据字典明细表';

/*Data for the table `myblog_dictionay_info` */

/*Table structure for table `myblog_label` */

DROP TABLE IF EXISTS `myblog_label`;

CREATE TABLE `myblog_label` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `ARTICLE_ID` int(11) DEFAULT NULL COMMENT '文章ID',
  `LABEL_INFO_ID` int(11) DEFAULT NULL COMMENT '标签ID',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATED_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文章标签中间表';

/*Data for the table `myblog_label` */

/*Table structure for table `myblog_label_info` */

DROP TABLE IF EXISTS `myblog_label_info`;

CREATE TABLE `myblog_label_info` (
  `ID` int(11) DEFAULT NULL COMMENT '主键ID',
  `CONNENT` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标签内容',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATED_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文章标签';

/*Data for the table `myblog_label_info` */

/*Table structure for table `myblog_resources` */

DROP TABLE IF EXISTS `myblog_resources`;

CREATE TABLE `myblog_resources` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `RESOURCES_CODE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '子源代码',
  `PARENT_ID` bigint(20) DEFAULT NULL COMMENT '父id',
  `URL` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '资源路径',
  `URL_DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '资源描述',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `myblog_resources` */

insert  into `myblog_resources`(`ID`,`RESOURCES_CODE`,`PARENT_ID`,`URL`,`URL_DESCRIPTION`,`DELETE_TIME`,`UPDATE_TIME`,`CREATE_TIME`) values 
(1,'user:manger',NULL,'/mybloguser','用户管理',NULL,'2021-06-28 09:46:14','2021-06-27 10:10:24'),
(2,'user:add',1,'/mybloguser','用户添加',NULL,'2021-06-28 09:49:22','2021-06-28 09:46:35'),
(3,'user:update',1,'/mybloguser','用户更新',NULL,'2021-06-28 09:49:23','2021-06-28 09:48:58'),
(4,'user:findAll',1,'/mybloguser','用户查询',NULL,'2021-06-28 09:49:25','2021-06-28 09:49:20');

/*Table structure for table `myblog_role_resources` */

DROP TABLE IF EXISTS `myblog_role_resources`;

CREATE TABLE `myblog_role_resources` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `ROLE_ID` bigint(20) DEFAULT NULL COMMENT '权限id',
  `RESOURCES_ID` bigint(20) DEFAULT NULL COMMENT '资源id',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `myblog_role_resources` */

insert  into `myblog_role_resources`(`ID`,`ROLE_ID`,`RESOURCES_ID`,`DELETE_TIME`,`UPDATE_TIME`,`CREATE_TIME`) values 
(1,2,1,NULL,'2021-07-08 16:36:22','2021-06-27 10:09:47'),
(2,2,2,NULL,'2021-07-08 16:36:23','2021-06-28 09:49:54'),
(3,2,3,NULL,'2021-07-08 16:36:24','2021-06-28 09:50:03'),
(4,2,4,NULL,'2021-07-08 16:36:25','2021-06-28 09:50:10');

/*Table structure for table `myblog_roles` */

DROP TABLE IF EXISTS `myblog_roles`;

CREATE TABLE `myblog_roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `RNAME` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名字0用户1是管理员2违规',
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色信息';

/*Data for the table `myblog_roles` */

insert  into `myblog_roles`(`ID`,`RNAME`,`DESCRIPTION`,`DELETE_TIME`,`UPDATE_TIME`,`CREATE_TIME`) values 
(1,'ROLE_USER','用户',NULL,'2021-06-28 16:03:33','2020-07-29 17:33:27'),
(2,'ROLE_ADMIN','管理员',NULL,'2021-06-28 16:03:39','2020-07-29 17:33:27');

/*Table structure for table `myblog_system` */

DROP TABLE IF EXISTS `myblog_system`;

CREATE TABLE `myblog_system` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `NUMBER` int(11) DEFAULT NULL COMMENT '用户游览量',
  `CREATED_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `RECORD` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备案信息',
  `META_CONNECT` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'META',
  `TITLE` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '设置标题',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATED_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统设置';

/*Data for the table `myblog_system` */

/*Table structure for table `myblog_user` */

DROP TABLE IF EXISTS `myblog_user`;

CREATE TABLE `myblog_user` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `USERNAME` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `PASSWORD` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `EMAIL_ADDRESS` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱地址',
  `ACTIVATION_CODE` varchar(255) DEFAULT NULL COMMENT '找回密码凭证',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE KEY `USERNAME` (`USERNAME`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户信息';

/*Data for the table `myblog_user` */

insert  into `myblog_user`(`ID`,`USERNAME`,`PASSWORD`,`EMAIL_ADDRESS`,`ACTIVATION_CODE`,`DELETE_TIME`,`UPDATE_TIME`,`CREATE_TIME`) values 
(1,'2417672197','$2a$10$b9q2xY16mAYj7.AtBES8d.qhC7KYCkHpmQdtzowGfQmGUpMp7fGUy','2417672197@qq.com',NULL,NULL,'2021-07-08 11:54:59','2021-07-08 11:54:59');

/*Table structure for table `myblog_user_roles` */

DROP TABLE IF EXISTS `myblog_user_roles`;

CREATE TABLE `myblog_user_roles` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `USER_ID` bigint(20) DEFAULT NULL COMMENT '用户id',
  `ROLE_ID` bigint(20) DEFAULT NULL COMMENT '角色id',
  `DELETE_TIME` datetime DEFAULT NULL COMMENT '删除时间',
  `UPDATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `myblog_user_roles` */

insert  into `myblog_user_roles`(`ID`,`USER_ID`,`ROLE_ID`,`DELETE_TIME`,`UPDATE_TIME`,`CREATE_TIME`) values 
(1,1,1,NULL,'2021-07-10 18:12:37','2021-06-27 10:09:01');

/*Table structure for table `pdman_db_version` */

DROP TABLE IF EXISTS `pdman_db_version`;

CREATE TABLE `pdman_db_version` (
  `DB_VERSION` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `VERSION_DESC` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CREATED_TIME` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `pdman_db_version` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
